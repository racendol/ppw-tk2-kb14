from django import forms

class KomentarForm(forms.Form):
    attrs = {
        'type':'text',
        'class': 'form-control',
        'id':'form-komentar',
    }

    komentar = forms.CharField(label = "Berikan Komentarmu!", max_length=50, required=True, widget=forms.TextInput(attrs=attrs))
