from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import admin
from .apps import AboutConfig
from .models import Komentar
from .forms import KomentarForm
from .views import index, create_comment, delete_comment

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from django.contrib.auth.models import User

import time
# Create your tests here

#url, forms, models, views, template
class UnitTest(TestCase):
    def test_if_index_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, index)

    def test_if_create_comment_url_is_exist(self):
        response = Client().get('/about/create_comment/')
        self.assertEqual(response.status_code, 302)

    def test_create_comment_url_using_func(self):
        found = resolve('/about/create_comment/')
        self.assertEqual(found.func, create_comment)

    def test_if_delete_comment_url_is_exist(self):
        response = Client().get('/about/delete_comment/1')
        self.assertEqual(response.status_code, 302)

    def test_delete_comment_url_using_func(self):
        found = resolve('/about/delete_comment/1')
        self.assertEqual(found.func, delete_comment)

    def test_if_url_is_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    
    def test_form_is_valid(self):
        form_data = {'komentar':'Sebuah Komentar'}
        form = KomentarForm(data = form_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['komentar'], 'Sebuah Komentar')

    def test_form_is_not_valid(self):
        form = KomentarForm(data = {})
        self.assertFalse(form.is_valid())

    def test_index_use_right_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_index_does_not_have_komentar(self):
        response = Client().get('/about/')
        self.assertContains(response, 'Belum ada komentar')
    
    def test_index_have_komentar(self):
        komentar = Komentar.objects.create(komentar='tes')
        komentar.save()
        response = Client().get('/about/')
        self.assertContains(response, 'tes')
    
    def test_index_create_komentar_valid(self):
        response = Client().post('/about/create_comment/', {'komentar':'Sebuah Komentar'})
        self.assertRedirects(response, '/about/')
        
        response2 = Client().get('/about/')
        self.assertContains(response2, 'Sebuah Komentar')

    def test_index_create_komentar_not_valid(self):
        response = Client().post('/about/create_comment/', {})
        self.assertRedirects(response, '/about/')

        response2 = Client().get('/about/')
        self.assertContains(response2, 'Belum ada komentar')

    # def test_index_delete_komentar_valid(self):
    #     Client().post('/about/create_comment/', {'komentar':'Sebuah Komentar'})
    #     response = Client().get('/about/delete_comment/1')
    #     self.assertRedirects(response, '/about/')
        
    #     response2 = Client().get('/about/')
    #     self.assertContains(response2, 'Belum ada komentar')

    # def test_index_delete_komentar_not_valid(self):
    #     Client().post('/about/create_comment/', {'komentar':'Sebuah Komentar'})
    #     response = Client().get('/about/delete_comment/2')
    #     self.assertRedirects(response, '/about/')
        
        # response2 = Client().get('/about/')
        # self.assertNotContains(response2, 'Belum ada komentar')
        # self.assertContains(response2, 'Sebuah Komentar')

    
class KomentarModelsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Komentar.objects.create(komentar="Sebuah Komentar")

    def test_if_komentar_created(self):
        count = Komentar.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_if_komentar_exist(self):
        komentar = Komentar.objects.get(id=1)
        komentar_isi = komentar.komentar
        self.assertEqual(komentar_isi, "Sebuah Komentar")

    def test_if_komentar_max_lenth_is_50(self):
        komentar = Komentar.objects.get(id=1)
        komentar_length = komentar._meta.get_field('komentar').max_length
        self.assertEqual(komentar_length, 50)

    def test_if_new_is_in_database(self):
        new_komentar = Komentar(komentar = 'tes')
        new_komentar.save()

        count = Komentar.objects.all().count()

        self.assertEqual(count, 2)
    

    
class FunctionalTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--start-maximized")
        cls.selenium = webdriver.Chrome(chrome_options=chrome_options)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


    def test_login_and_add_komentar(self):

        self.selenium.get(self.live_server_url + '/about/')
        time.sleep(2)

        self.assertNotIn('Berikan Komentarmu!', self.selenium.page_source)

        self.selenium.get(self.live_server_url + '/login/')

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertIn('Login', navbar.get_attribute('innerHTML'))

        self.selenium.find_element_by_id('id_username').send_keys('tes_user')
        self.selenium.find_element_by_id('id_password').send_keys('tes_password')
        self.selenium.find_element_by_id('login-button').click()
        time.sleep(3)

        self.assertIn('ID / Password salah atau belum terdaftar', self.selenium.page_source)

        user = User.objects.create_user('tes_user', 'tes@tes.com', 'tes_password')
        user.save()

        self.selenium.find_element_by_id('id_username').clear()
        self.selenium.find_element_by_id('id_password').clear()
        self.selenium.find_element_by_id('id_username').send_keys('tes_user')
        self.selenium.find_element_by_id('id_password').send_keys('tes_password')

        self.selenium.find_element_by_id('login-button').click()
        time.sleep(2)

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertNotIn('Login', navbar.get_attribute('innerHTML'))
        self.assertIn('Logout', navbar.get_attribute('innerHTML'))

        #tes add komentar
        self.selenium.get(self.live_server_url + '/about/')
        time.sleep(2)
        
        comment_form = self.selenium.find_element_by_id('form-komentar')
        comment_form.send_keys('oke')

        self.selenium.find_element_by_id('buat-komentar-button').click()
        time.sleep(5)

        self.assertIn('oke', self.selenium.page_source)

        #tes logout
        time.sleep(3)
        logout_button = self.selenium.find_element_by_id('logout-button')
        logout_button.click()
        

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertIn('Login', navbar.get_attribute('innerHTML'))
        self.assertNotIn('Logout', navbar.get_attribute('innerHTML'))


