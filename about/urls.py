from django.urls import path
from .views import index, create_comment, delete_comment

urlpatterns = [
    path('', index, name = 'about'),
    path('create_comment/', create_comment, name = 'create_comment'),
    path('delete_comment/<int:komentar_id>', delete_comment, name = 'delete_comment')
]