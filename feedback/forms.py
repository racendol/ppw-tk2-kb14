from django import forms
from .models import Feedback

class FeedbackForm(forms.Form):
    attrs = {
        'type':'text',
        'class': 'form-control big-input',
    }

    nama = forms.CharField(label = "Nama", max_length=50, required=True, widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label = "Email", max_length=50, required=True, widget=forms.TextInput(attrs=attrs))
    isi = forms.CharField(label = "Feedback anda", max_length=50, required=True, widget=forms.TextInput(attrs=attrs))

