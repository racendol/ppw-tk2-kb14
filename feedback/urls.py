from django.urls import path
from .views import index, create_feedback

urlpatterns = [
    path('', index, name = 'feedback'),
    path('create_feedback/', create_feedback, name = 'create_feedback'),
]