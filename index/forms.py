from django import forms
from django.forms import Form
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User


KOTA_CHOICES = [
	("------","------"),
	('Jakarta', 'Jakarta'),
	('Surabaya','Surabaya'),
	('Palu','Palu')
	]

class Info_Form(forms.Form):

	pos_attrs = {
		'class':'form-control',
	}
	kota = forms.CharField(label='Pilih lokasi Anda', required=True, widget=forms.Select(choices=KOTA_CHOICES))
	
	suhu = forms.CharField(label='Suhu Udara', required=True)
	udara = forms.CharField(label='Kualitas Udara', required=True)
	kebakaran = forms.CharField(label='Kebakaran', required=False)
	banjir = forms.CharField(label='Banjir', required=False)
	gempa = forms.CharField(label='Gempa Bumi', required=False)
	peringatan = forms.CharField(required=False)
	langkah = forms.CharField(widget=forms.Textarea(attrs=pos_attrs), required=False, max_length=500)

class KotaForm(forms.Form):
	attrs = {
		'class':'form-control',
		'id':'input-kota'
	}
	
	kota_input = forms.CharField(label='Pilih lokasi Anda', required=True, widget=forms.TextInput(attrs=attrs), max_length=20)

class CustomAuthForm(AuthenticationForm):
	attrs = {
		'class':'form-control'
	}

	username = forms.CharField(widget=forms.TextInput(attrs=attrs))
	password = forms.CharField(widget=forms.PasswordInput(attrs=attrs))

class SignUpForm(UserCreationForm):
	first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
	contact = forms.CharField(max_length= 20, required=True)

	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'contact',)