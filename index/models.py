from django.db import models
from django.contrib.auth.models import User

from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Favorite(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	kota = models.CharField(blank=True, null=True, max_length=20)


"""
Fungsi ketika sebuah object user dibuat/disave, maka favorite akan otomatis dibuat/disave
Referenced from: https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html
"""
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Favorite.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.favorite.save()
