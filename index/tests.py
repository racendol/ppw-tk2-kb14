from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import admin
from .views import index

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import time

from django.contrib.auth.models import User

# Create your tests here.


class UnitTest(TestCase):
    def test_if_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_if_url_is_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    def test_index_use_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')


class FunctionalTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--start-maximized")
        cls.selenium = webdriver.Chrome(chrome_options=chrome_options)
        # cls.selenium = webdriver.Chrome()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_landing_page(self):
        self.selenium.get(self.live_server_url)

        time.sleep(5)

        kota_form = self.selenium.find_element_by_id('input-kota')
        kota_form.send_keys('surabaya')

        self.selenium.find_element_by_id('form-kota-button').click()

        time.sleep(5)
        kota_form.clear()
        self.assertIn('Surabaya', self.selenium.page_source)

    def test_login_and_save_favorite(self):

        self.selenium.get(self.live_server_url + '/login/')

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertIn('Login', navbar.get_attribute('innerHTML'))

        self.selenium.find_element_by_id('id_username').send_keys('tes_user')
        self.selenium.find_element_by_id('id_password').send_keys('tes_password')
        self.selenium.find_element_by_id('login-button').click()
        time.sleep(3)

        self.assertIn('ID / Password salah atau belum terdaftar', self.selenium.page_source)

        user = User.objects.create_user('tes_user', 'tes@tes.com', 'tes_password')
        user.save()

        self.selenium.find_element_by_id('id_username').clear()
        self.selenium.find_element_by_id('id_password').clear()
        self.selenium.find_element_by_id('id_username').send_keys('tes_user')
        self.selenium.find_element_by_id('id_password').send_keys('tes_password')

        self.selenium.find_element_by_id('login-button').click()
        time.sleep(2)

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertNotIn('Login', navbar.get_attribute('innerHTML'))
        self.assertIn('Logout', navbar.get_attribute('innerHTML'))

        #tes favorite
        self.selenium.get(self.live_server_url)
        time.sleep(2)
        
        kota_form = self.selenium.find_element_by_id('input-kota')
        kota_form.send_keys('Bekasi')

        self.selenium.find_element_by_id('save-favorite-button').click()
        time.sleep(5)

        kota_form.clear()
        self.assertIn('Kota berhasil disimpan sebagai favorit', self.selenium.page_source)

        self.selenium.find_element_by_id('get-favorite-button').click()
        time.sleep(5)

        self.assertIn('Kota favorit adalah: Bekasi', self.selenium.page_source)

        #tes logout
        time.sleep(3)
        logout_button = self.selenium.find_element_by_id('logout-button')
        logout_button.click()
        

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertIn('Login', navbar.get_attribute('innerHTML'))
        self.assertNotIn('Logout', navbar.get_attribute('innerHTML'))