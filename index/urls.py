from django.urls import path
from . import views
from .views import index, get_info, get_ip_loc, save_favorite, get_favorite

urlpatterns = [
    path('', index, name = 'index'),
    path('get_info/', get_info, name = 'get_info'),
    path('get_ip_loc/', get_ip_loc, name = 'get_ip_loc'),
    path('save_favorite/', save_favorite),
    path('get_favorite/', get_favorite)
]