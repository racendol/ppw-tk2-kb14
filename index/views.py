from django.shortcuts import render
from django.shortcuts import redirect, HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from .forms import Info_Form, KotaForm
import requests
from .forms import SignUpForm
from django.contrib.auth import login, authenticate

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect('/')
    else:
        form = SignUpForm()
    return render(request, 'register.html', {'form': form})


# Create your views here.

def index(request):
	response = {'form':KotaForm}

	return render(request, 'home.html', response)

def get_info(request):
	kota = request.GET.get('kota', None)

	if (kota == '' or kota == None):
		kota = 'Jakarta'

	kota = kota.capitalize()

	url_kota = "https://api.opencagedata.com/geocode/v1/json?key=35f7daf076994ec2a929105e6f0bc625&q="+kota+"&pretty=1"
	result_kota = requests.get(url_kota).json()

	results = result_kota['results']
	kota_res = dict()
	offset = 0

	for res in results:
		if res['components']['country'] == 'Indonesia':
			offset = res['annotations']['timezone']['offset_sec']
			offset = offset / 3600
			offset -= 7 #kalo gmt+7, offset = 0. gmt+8 offset = 1. etc.
			break
	else:
		#not found, default ke jakarta
		kota = 'Jakarta'

	url_weather = "http://api.openweathermap.org/data/2.5/weather?q="+kota+"&appid=fb1b419db533629eeece714f384e5048"
	result_weather = requests.get(url_weather).json()


	temp = result_weather['main']['temp']
	#temp somehow disimpan jadi A00.BC  celcius, jadi harus di bagi 10, lalu diformat agar bisa 2 angka belakang desimal
	temp = "{0:.2f}".format(temp/10)

	humidity = result_weather['main']['humidity']
	
	#karena gak ada data kondisi, maka kondisinya adalah ascii dari huruf pertama kota di mod 4
	#0 = gak ada, 1 = banjir, 2 = kebakaran, 3 = gempa bumi
	kondisi = ord(kota[0]) % 4


	final_dict = {
		'nama': kota,
		'offset':offset,
		'temp':temp,
		'humidity':humidity,
		'kondisi':kondisi
	}
	
	return JsonResponse(final_dict)

def get_ip_loc(request):
	url = 'http://ip-api.com/json/?fields=city,country'
	result = requests.get(url).json()

	return JsonResponse(result)

def save_favorite(request):
	response = {}

	if request.method == 'POST' and request.user.is_authenticated:
		kota = request.POST['kota']
		if kota == None or kota == '' or kota.strip() == '':
			response['success'] = False
			response['error_message'] = 'Masukkan tidak boleh kosong'

		else:
			request.user.favorite.kota = kota
			request.user.save()
			response['success'] = True

	else:
		response['success'] = False
		response['error_message'] = 'Something went wrong'

	return JsonResponse(response)
	

def get_favorite(request):
	response = {}
	if request.user.is_authenticated:
		kota = request.user.favorite.kota
		if kota == '' or kota == None:
			response['success'] = False
			response['error_message'] = 'Belum mempunyai kota favorit'
		else:
			response['favorite'] = kota
			response['success'] = True
	else:
		response['success'] = False
		response['error_message'] = 'Something went wrong'

	return JsonResponse(response)
