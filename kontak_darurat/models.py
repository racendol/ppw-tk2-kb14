from django.db import models

# Create your models here.
class Form(models.Model):
    nama = models.CharField(blank= False, max_length=50)
    email = models.EmailField(blank= False, max_length=254)
    kontak = models.CharField(max_length=15)
    alamat = models.CharField(blank= False, max_length=200)
    kota = models.CharField(blank= False, max_length=50)
    Deskripsi_Kedaruratan = models.TextField(blank= False, max_length=2000)