from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .views import index

import time
import unittest
import os


class ReportsConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(apps.get_app_config('kontak_darurat').name, 'kontak_darurat')

class UntitTest(TestCase):
    
    def test_if_index_url_is_exist(self):
        response = Client().get('/kontak_darurat/')
        self.assertEquals(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/kontak_darurat/')
        self.assertEquals(found.func, index)

    def test_if_using_template(self):
        response = Client().get('/kontak_darurat/')
        self.assertTemplateUsed(response, 'kontak_darurat.html')

    def test_if_using_templates_forms(self):
        response = Client().get('/kontak_darurat/add_forms/')
        self.assertTemplateUsed(response, 'forms.html')
