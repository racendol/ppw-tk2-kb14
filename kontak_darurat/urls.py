from django.urls import path
from .views import index, add_forms, get_ip_loc

# app_name = "kontak_darurat"

urlpatterns = [
    path('', index, name = 'kontak_darurat'),
    path('add_forms/', add_forms, name='forms_kontak_darurat'),
    path('get_ip_loc/', get_ip_loc, name = 'get_ip_loc'),
]