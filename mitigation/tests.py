from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import admin
from .models import Bencana
from .forms import BencanaForm
from .views import index


# Create your tests here.

class UnitTest(TestCase):
    def test_if_index_url_is_exist(self):
        response = Client().get('/mitigation/')
        self.assertEqual(response.status_code, 200)

    def test_index_url_using_func(self):
        found = resolve('/mitigation/')
        self.assertEqual(found.func, index)

    def test_if_url_is_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    def test_form_is_not_valid(self):
        form = BencanaForm(data = {})
        self.assertFalse(form.is_valid())

    def test_index_use_right_template(self):
        response = Client().get('/mitigation/')
        self.assertTemplateUsed(response, 'mitigation.html')


