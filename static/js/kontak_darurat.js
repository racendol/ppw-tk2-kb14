var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}


$(document).ready(function () {
        getIPLocation();

        $("submit").submit(function (event) {
        event.preventDefault(); //Disable auto refresh ketika submit
    })
})

function getIPLocation() {
    var text = '';
    $.ajax({
        url:'/get_ip_loc/',
        success:function (result) {
            kota = result['city']
            country = result['country']

            if (country != 'Indonesia') {
                kota = 'Jakarta'
            }

            text += '<h3>' + kota + ',' + '</h3>' +
            '<h3>' + country + '</h3>'

            $('#kota').append(
                text
            )
        },
    })
}
