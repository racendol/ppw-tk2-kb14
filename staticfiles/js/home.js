var currClock;
var currCity;

$(document).ready(function () {
    // kota_list = [
    //     'Aceh','Banda Aceh','Langsa', 'Lhokseumawe', 'Meulaboh', 'Sabang', 'Subulussalam',
    //     'Bali','Denpasar', 'Pangkalpinang', 'Bangka Belitung', 'Cilegon', 'Serang', 'Banten', 'Tangerang',
    //     'Bengkulu', 'Gorontalo', 'Jakarta', 'Jambi', 'Sungai Penuh', 'Jawa Barat', 'Bandung', 'Bekasi',
    //     'Bogor', 'Cimahi', 'Cirebon', 'Depok', 'Sukabumi', 'Tasikmalaya', 'Banjar', 'Magelang', 'Purwokerto',
    //     'Salatiga', 'Semarang', 'Surakarta', 'Tegal', 'Batu', 'Blitar', 'Kediri', 'Madiun', 'Malang', 'Pojokerto',
    //     'Pasuruan', 'Probolinggo', 'Surabaya', 'Pontianak', 'Singkawang', 'Banjarbaru', 'Banjarmasin', 'Palangkaraya',
    //     'Balikpapan', 'Bontang', 'Samarinda', 'Tarakan', 'Batam', 'Tanjungpinang', 'Bandar Lampung', 'Metro', 'Lampung',
    //     'Ternate', 'Tidore', 'Ambon', 'Tuai', 'Maluku', 'Bima', 'Mataram', 'Kupang', 'Sorong', 'Jayapura', 'Dumai', 'Pekanbaru',
    //     'Makassar', 'Palopo', 'Riau', 'Parepare', 'Palu', 'Bau-Bau', 'Kendari', 'Bitung', 'Kotamobagu', 'Manado','Tomohon',
    //     'Bukittinggi', 'Padang', 'Padangpanjang', 'Pariaman', 'Sayakumbuh', 'Sawahlunto', 'Solok', 'Lubuklinggau', 'Pagaralam',
    //     'Palembang', 'Prabumulih', 'Binjai', 'Medan', 'Padang Sidempuan', 'Sibolga', 'Tanjungbalai', 'Tebingtinggi', 'Yogyakarta'
    // ]

    getIPLocation();
    
    $("#form-kota").submit(function (event) {
        event.preventDefault(); //Disable auto refresh ketika submit

        kota = $("#input-kota").val();
        getInfo(kota);

    })


    $("#save-favorite-button").click(function (event) {
        kota = $("#input-kota").val();

        $.ajax({
            url:'/save_favorite/',
            data:{
                'kota':kota,
                'csrfmiddlewaretoken': csrfToken
            },
            method:"POST",
            beforeSend: showAjaxImage(),
            success: function (result) {
                status = result['success']
                if (!status) {
                    $("#ajax-response-text").text(result['error_message'])
                } else {
                    $("#ajax-response-text").text("Kota berhasil disimpan sebagai favorit")
                }
                hideAjaxImage()
            }
        })
    })


    $("#get-favorite-button").click(function (event) {
        $.ajax({
            url:'/get_favorite/',
            beforeSend: showAjaxImage(),
            success: function (result) {
                status = result['success']
                if (!status) {
                    $("#ajax-response-text").text(result['error_message'])
                } else {
                    kota = result['favorite']
                    $("#ajax-response-text").text("Kota favorit adalah: " + kota)
                }
                hideAjaxImage()
            }
        })
    })

    $("#dropdown_image").click(function () {
        $("#steps").slideToggle();
    })

})

function hideAjaxImage() {
    $("#ajax-image").hide();
}

function showAjaxImage(){
    $("#ajax-image").show();
}

function getInfo(kota) {
    if (typeof kota !== 'string' || kota == '') {
        kota = currCity;
    }

    $.ajax({
        url:'/get_info/',
        data:{
            'kota':kota
        },
        beforeSend: showAjaxImage(),
        success: function (result) {
            nama_kota = result['nama']
            offset = result['offset']
            humidity = result['humidity']
            temp = result['temp']
            kondisi = result['kondisi']

            $("#text-kota").text(nama_kota);
            $("#text-humidity").text(humidity);
            $("#text-suhu-udara").text(temp+"°C");

            $("#text-kebakaran").next().hide()
            $("#text-banjir").next().hide()
            $("#text-gempa").next().hide()
            $("#dropdown_image").next().hide()


            $("#warning-isi").empty()

            if (kondisi == 1) {
                $("#text-kebakaran").next().show()
                $("#dropdown_image").next().show()
                $("#warning-judul").text("Kebakaran sedang terjadi!")
                $("#warning-isi").append(
                    "<h2 style='color: #fdbaba;'> Segera keluar melalui pintu darurat.</h2><br>"+
                    "<h2> Nyalakan alarm dan cari bantuan.</h2><br>"+
                    "<h2 style='color: #fdbaba;'> Hati-hati dengan gas yang bocor dan kabel yang terbuka.</h2><br>"
                    )

            } else if(kondisi == 2){
                $("#text-banjir").next().show()
                $("#dropdown_image").next().show()
                $("#warning-judul").text("Banjir sedang terjadi!")
                $("#warning-isi").append(
                    "<h2 style='color: #fdbaba;'> Padamkan listrik.</h2><br>"+
                    "<h2>Matikan gas.</h2><br>"+
                    "<h2 style='color: #fdbaba;'>Mengungsi ke dataran yang lebih tinggi.</h2><br>"+
                    "<h2>Jangan berjalan di tengah banjir.</h2>"
                    )

            } else if(kondisi == 3){
                $("#text-gempa").next().show()
                $("#dropdown_image").next().show()
                $("#warning-judul").text("Gempa bumi sedang terjadi!")
                $("#warning-isi").append(
                    "<h2 style='color: #fdbaba;'>Jauhi jendela, kaca, dan benda berat.</h2><br>"+
                    "<h2>Padamkan api segera, jika sedang menyalakan api.</h2><br>"+
                    "<h2 style='color: #fdbaba;'>Jika sedang di tempat tidur, jangan bergerak, letakkan bantal di atas kepala.</h2><br>"+
                    "<h2> Jika sedang berada dalam gedung,"+
                    "<br><span style='color: #fdbaba;>berlindung di bawah meja yang kokoh.</span>"+
                    "<br>Tunggu hingga getaran berhenti, lalu segera keluar gedung.</h2>"
                    )

            } else {
                $("#warning-isi").append(
                    "<h2>Tidak ada bencana</h2>"
                    )
            }

            clearInterval(currClock);
            
            displayClock(offset);
            currClock = setInterval(displayClock, 1000, offset);
            hideAjaxImage()
        }
    })

}

function getIPLocation() {
    $.ajax({
        url:'/get_ip_loc/',
        success:function (result) {
            kota = result['city']
            country = result['country']

            if (country != 'Indonesia') {
                kota = 'Jakarta'
            }

            currCity = kota;
        },
        complete: function () {
            getInfo(currCity);
        }
    })
}

function displayClock(offset) {
    var date = new Date();
    var day_list = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
    var month_list = [
        'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
        'September', 'Oktober', 'November', 'Desember'
    ]

    function addZero(num) {
        if (num < 10) {
            return "0" + num;
        } else {
            return num;
        }
    }
    
    var hour =  date.getHours() + offset;
    var min = addZero(date.getMinutes());
    var sec = addZero(date.getSeconds());

    var day = date.getDay();
    var day_date = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();

    if (hour > 24) { //handling kalau ganti hari
        day = day+1 % 7;

        day_date++;

        if (day_date > 30) {
            if (month > 6 && month % 2 == 0) {
                month++;
                day_date = 1;

            } else if (month <= 6 && month % 2 == 1){
                month++;
                day_date = 1;
            }

        } else if (day_date > 31) {
            month++;
            day_date = 1;
        }
        
        if (month == 1 && day_date > 28) {
            if (year % 4 != 0) {
                month++;
                day_date = 1;
            } else if(day_date > 29){
                month++;
                day_date = 1;
            }
        }

        if (month > 11) {
            year++;
            month = 0;
        }
    }

    hour = addZero(hour);
    month = month_list[month];
    day = day_list[day];

    $("#text-hari").text(day + ",");
    $("#text-tanggal").text(day_date + " "+ month + " " + year);
    $("#text-jam").text(hour+ ":" + min + ":" + sec);
}

// /**
//  * Filter array items based on search criteria (query)
//  * Referenced from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
//  */
// function filterItems(arr, query) {
//     return arr.filter(function(el) {
//         return el.toLowerCase().indexOf(query.toLowerCase()) !== -1;
//     })
// }